'use strict';


/**
 * Add a new user
 * Adds a new user and returns the added user
 *
 * body User The new user to add (optional)
 * returns User
 **/
exports.addUsers = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "userId" : 0,
  "firstName" : "John",
  "lastName" : "Doe",
  "phoneNumber" : "555-555-5555",
  "address" : "US"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete a specific user
 * Removes a user entry from the system
 *
 * userId String The unique user id
 * returns User
 **/
exports.deleteUser = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "userId" : 0,
  "firstName" : "John",
  "lastName" : "Doe",
  "phoneNumber" : "555-555-5555",
  "address" : "US"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get a specific user
 * Returns the user details
 *
 * userId String The unique user id
 * returns User
 **/
exports.getUser = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "userId" : 0,
  "firstName" : "John",
  "lastName" : "Doe",
  "phoneNumber" : "555-555-5555",
  "address" : "US"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all the users
 * Returns the list of all users
 *
 * sortBy String The sort order
 * returns List
 **/
exports.getUsers = function(sortBy) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "userId" : 0,
  "firstName" : "John",
  "lastName" : "Doe",
  "phoneNumber" : "555-555-5555",
  "address" : "US"
}, {
  "userId" : 0,
  "firstName" : "John",
  "lastName" : "Doe",
  "phoneNumber" : "555-555-5555",
  "address" : "US"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Update an exisiting user
 * Updates a user entry and returns the updated user
 *
 * body User The user details to update
 * userId String The unique user id
 * returns User
 **/
exports.updatedUser = function(body,userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "userId" : 0,
  "firstName" : "John",
  "lastName" : "Doe",
  "phoneNumber" : "555-555-5555",
  "address" : "US"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

