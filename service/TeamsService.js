'use strict';


/**
 * Add a new tean
 * Adds a new team and returns the added team
 *
 * body Team The new team to add (optional)
 * returns Team
 **/
exports.addTeams = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "teamId" : 0,
  "teamName" : "TestOps",
  "teamSize" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete a specific team
 * Removes a team entry from the system
 *
 * teamId String The unique team id
 * returns Team
 **/
exports.deleteTeam = function(teamId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "teamId" : 0,
  "teamName" : "TestOps",
  "teamSize" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get a specific team
 * Returns the team details
 *
 * teamId String The unique team id
 * returns Team
 **/
exports.getTeam = function(teamId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "teamId" : 0,
  "teamName" : "TestOps",
  "teamSize" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all the teams
 * Returns the list of all teams
 *
 * sortBy String The sort order
 * returns List
 **/
exports.getTeams = function(sortBy) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "teamId" : 0,
  "teamName" : "TestOps",
  "teamSize" : 10
}, {
  "teamId" : 0,
  "teamName" : "TestOps",
  "teamSize" : 10
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Update an exisiting team
 * Updates a team entry and returns the updated team
 *
 * body Team The team details to update
 * teamId String The unique team id
 * returns Team
 **/
exports.updatedTeam = function(body,teamId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "teamId" : 0,
  "teamName" : "TestOps",
  "teamSize" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

