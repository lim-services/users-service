'use strict';

var utils = require('../utils/writer.js');
var Teams = require('../service/TeamsService');

module.exports.addTeams = function addTeams (req, res, next, body) {
  Teams.addTeams(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteTeam = function deleteTeam (req, res, next, teamId) {
  Teams.deleteTeam(teamId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getTeam = function getTeam (req, res, next, teamId) {
  Teams.getTeam(teamId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getTeams = function getTeams (req, res, next, sortBy) {
  Teams.getTeams(sortBy)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updatedTeam = function updatedTeam (req, res, next, body, teamId) {
  Teams.updatedTeam(body, teamId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
